import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask

plugins {
    kotlin("jvm") version libs.versions.kotlin
    alias(libs.plugins.serialization)
    alias(libs.plugins.ktor)
    alias(libs.plugins.versions)
}

group = "hr.ech.journal"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.bundles.cucumber)
    implementation(libs.bundles.ktor.client)
    implementation(libs.assertk.jvm)
    implementation(libs.guice)
    implementation(libs.commons.lang3)
    implementation(libs.java.jwt)
    implementation(libs.passay)
    runtimeOnly(libs.kotlin.reflect)
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation(libs.cucumber.junit)
    testImplementation(libs.junit.vintage.engine)
    testImplementation(libs.kotlin.test.junit5)
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(18)
}

fun isNonStable(version: String): Boolean {
    val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { version.uppercase().contains(it) }
    val regex = "^[0-9,.v-]+(-r)?$".toRegex()
    val isStable = stableKeyword || regex.matches(version)
    return isStable.not()
}

tasks.withType<DependencyUpdatesTask> {
    rejectVersionIf {
        isNonStable(candidate.version) && !isNonStable(currentVersion)
    }
}
