package hr.ech.journal.cucumber

import org.apache.commons.lang3.RandomStringUtils
import org.passay.CharacterRule
import org.passay.EnglishCharacterData
import org.passay.PasswordGenerator
import java.util.Locale

const val STRING_SEGMENT_LENGTH = 3

fun randomString(): String =
    StringBuilder().append(
        randomLetters(STRING_SEGMENT_LENGTH).lowercase(),
        randomLetters(STRING_SEGMENT_LENGTH).uppercase(),
        randomNumbers(2),
    ).toString()

fun randomUsername() =
    StringBuilder().append(
        randomLetters(STRING_SEGMENT_LENGTH).lowercase(),
        randomLetters(STRING_SEGMENT_LENGTH).lowercase().capitalize(),
        randomNumbers(2),
    ).toString()

fun randomLetters(count: Int): String =
    RandomStringUtils.random(
        count,
        true,
        false,
    )

fun randomNumbers(count: Int): String =
    RandomStringUtils.random(
        count,
        false,
        true,
    )

fun String.capitalize() =
    replaceFirstChar {
        if (it.isLowerCase()) {
            it.titlecase(Locale.getDefault())
        } else {
            it.toString()
        }
    }

const val MINIMUM_PASSWORD_LENGTH = 8

fun randomPassword(): String =
    PasswordGenerator().generatePassword(
        MINIMUM_PASSWORD_LENGTH,
        listOf(
            CharacterRule(EnglishCharacterData.UpperCase, 1),
            CharacterRule(EnglishCharacterData.LowerCase, 1),
            CharacterRule(EnglishCharacterData.Digit, 1),
            CharacterRule(EnglishCharacterData.Special, 1),
        ),
    )
