package hr.ech.journal.cucumber.client

import com.google.inject.Inject
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.head
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.contentType
import kotlinx.coroutines.runBlocking

const val JOURNAL_API_BASE_URL = "http://localhost:8080/api"

class JournalApiClient @Inject constructor(
    private val httpClient: HttpClient,
) {
    fun registerWithResponse(
        username: String,
        displayName: String,
        password: String,
    ) = runBlocking {
        registerInternal(username, displayName, password).let {
            Pair(it.status, it.body<RegisterResponseDto>())
        }
    }

    private suspend fun registerInternal(
        username: String,
        displayName: String,
        password: String,
    ) = httpClient.post("$JOURNAL_API_BASE_URL/register") {
        contentType(ContentType.Application.Json)
        setBody(
            RegisterDto(
                username = username,
                displayName = displayName,
                password = password,
            ),
        )
    }

    fun registerStatusOnly(
        username: String,
        displayName: String,
        password: String,
    ) = runBlocking {
        registerInternal(username, displayName, password).status
    }

    fun userExists(username: String) = runBlocking {
        httpClient.head("$JOURNAL_API_BASE_URL/users/exists?username=$username").status == HttpStatusCode.OK
    }

    fun login(
        username: String,
        password: String,
    ) = runBlocking {
        httpClient.post("$JOURNAL_API_BASE_URL/login") {
            contentType(ContentType.Application.Json)
            setBody(
                LoginDto(
                    username = username,
                    password = password,
                ),
            )
        }.body<TokenDto>().token
    }

    fun getEnvironments(token: String) = runBlocking {
        httpClient.get("$JOURNAL_API_BASE_URL/environments") {
            headers["Authorization"] = "Bearer $token"
        }.body<List<EnvironmentsDto>>()
    }
}
