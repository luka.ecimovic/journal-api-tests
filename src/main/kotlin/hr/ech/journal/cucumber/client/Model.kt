package hr.ech.journal.cucumber.client

import kotlinx.serialization.Serializable
import java.time.Instant
import java.util.UUID

@Serializable
data class RegisterDto(
    val username: String,
    val displayName: String,
    val password: String,
)

@Serializable
data class LoginDto(
    val username: String,
    val password: String,
)

@Serializable
data class TokenDto(
    val token: String,
)

@Serializable
data class EnvironmentsDto(
    val id: String,
    val name: String,
    @Serializable(with = InstantSerializer::class)
    val createdAt: Instant,
    val description: String,
    val users: List<EnvironmentUserDto>,
)

@Serializable
data class UserDto(
    val id: String,
    val username: String,
    val displayName: String,
)

@Serializable
data class EnvironmentUserDto(
    val username: String,
)

@Serializable
data class RegisterResponseDto(
    @Serializable(with = UUIDSerializer::class)
    val id: UUID,
)
