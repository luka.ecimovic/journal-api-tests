package hr.ech.journal.cucumber.config.guice

import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Singleton
import hr.ech.journal.cucumber.steps.generic.RequestContext
import hr.ech.journal.cucumber.steps.user.UserContext
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.http.ContentType
import io.ktor.serialization.kotlinx.json.json

class ApiModule : AbstractModule() {
    @Provides
    @Singleton
    fun provideHttpClient() = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(
                contentType = ContentType.Application.Json,
            )
        }
    }
}
