package hr.ech.journal.cucumber.config.guice

import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Singleton
import hr.ech.journal.cucumber.steps.generic.RequestContext
import hr.ech.journal.cucumber.steps.user.UserContext

class ContextModule: AbstractModule() {
    @Provides
    fun provideUserContext() = UserContext()

    @Singleton
    @Provides
    fun provideRequestContext() = RequestContext()
}