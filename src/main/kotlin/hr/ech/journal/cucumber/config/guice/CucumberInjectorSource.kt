package hr.ech.journal.cucumber.config.guice

import com.google.inject.Guice
import com.google.inject.Injector
import com.google.inject.Stage
import io.cucumber.guice.CucumberModules
import io.cucumber.guice.InjectorSource

class CucumberInjectorSource : InjectorSource {
    override fun getInjector(): Injector =
        Guice.createInjector(
            Stage.PRODUCTION,
            CucumberModules.createScenarioModule(),
            ApiModule(),
            ContextModule(),
        )
}
