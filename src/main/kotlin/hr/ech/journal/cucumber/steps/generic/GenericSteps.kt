package hr.ech.journal.cucumber.steps.generic

import assertk.assertThat
import assertk.assertions.containsOnly
import com.google.inject.Inject
import io.cucumber.guice.ScenarioScoped
import io.cucumber.java.ParameterType
import io.cucumber.java.en.Then
import io.ktor.http.HttpStatusCode

@ScenarioScoped
class GenericSteps @Inject constructor(
    private val requestContext: RequestContext
) {

    @ParameterType(".*")
    fun httpStatusCode(httpStatusCode: String) = HttpStatusCode.fromValue(httpStatusCode.toInt())

    @Then("Request fails with {int}")
    fun requestFailsWith(int1: Int) {
        assertThat(requestContext.httpStatusCodes).containsOnly(HttpStatusCode.fromValue(int1))
    }
}
