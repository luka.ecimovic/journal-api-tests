package hr.ech.journal.cucumber.steps.generic

import com.google.inject.Inject
import io.ktor.http.HttpStatusCode

class RequestContext @Inject constructor() {
    lateinit var httpStatusCodes: List<HttpStatusCode>
}
