package hr.ech.journal.cucumber.steps.user

import com.auth0.jwt.interfaces.DecodedJWT
import com.google.inject.Inject
import java.util.UUID

class UserContext @Inject constructor() {
    lateinit var tokenString: String

    lateinit var token: DecodedJWT

    lateinit var userId: UUID

    var displayName: String = ""

    fun username(): String = "$displayName@example.com"

    var password = ""
}
