package hr.ech.journal.cucumber.steps.user

import assertk.assertThat
import assertk.assertions.contains
import assertk.assertions.isEqualTo
import assertk.assertions.isFalse
import assertk.assertions.isTrue
import com.auth0.jwt.JWT
import com.google.inject.Inject
import hr.ech.journal.cucumber.client.JournalApiClient
import hr.ech.journal.cucumber.randomPassword
import hr.ech.journal.cucumber.randomUsername
import hr.ech.journal.cucumber.steps.generic.RequestContext
import io.cucumber.datatable.DataTable
import io.cucumber.guice.ScenarioScoped
import io.cucumber.java.ParameterType
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.ktor.http.HttpStatusCode
import io.cucumber.java.en.And

@ScenarioScoped
class UserSteps @Inject constructor(
    private val journalApiClient: JournalApiClient,
    private val userContext: UserContext,
    private val requestContext: RequestContext,
) {
    @ParameterType(".*")
    fun role(roleName: String?) = roleName

    @Given("User submits valid registration request")
    fun userSubmitsValidRegistrationRequest() {
        userContext.displayName = randomUsername()
        userContext.password = randomPassword()
        val (status, response) =
            journalApiClient.registerWithResponse(
                username = userContext.username(),
                displayName = userContext.displayName,
                password = userContext.password,
            )
        userContext.userId = response.id
        assertThat(
            status,
        ).isEqualTo(HttpStatusCode.OK)
    }

    @Then("User exists")
    fun userExists() {
        assertThat(journalApiClient.userExists(userContext.username())).isTrue()
    }

    @Then("User has Role {role}")
    fun userHasRole(role: String) {
        userContext.token.getClaim("roles").asList(String::class.java).contains(role)
    }

    @Then("User can log in")
    fun userCanLogIn() {
        userContext.tokenString =
            journalApiClient.login(
                username = userContext.username(),
                password = userContext.password,
            )
        userContext.token = JWT.decode(
            userContext.tokenString,
        )
        assertThat(
            userContext.token.getClaim("userId")?.asString(),
        ).isEqualTo(userContext.userId.toString())
    }

    @Then("User has default Environment")
    fun userHasDefaultEnvironment() {
        assertThat(
            journalApiClient.getEnvironments(userContext.tokenString).flatMap { environment ->
                environment.users.map { it.username }
            },
        ).contains(userContext.username())
    }

    @Given("User submits weak password")
    fun userSubmitsWeakPassword(dataTable: DataTable) {
        userContext.displayName = randomUsername()
        requestContext.httpStatusCodes = dataTable.asLists(String::class.java).map {
            journalApiClient.registerStatusOnly(
                username = userContext.username(),
                displayName = userContext.displayName,
                password = it[0],
            )
        }
    }

    @And("User doesn't exist")
    fun userDoesnTExist() {
        assertThat(journalApiClient.userExists(userContext.username())).isFalse()
    }
}
