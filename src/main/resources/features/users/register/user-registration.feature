@story:user-registration
Feature: User registration

    Scenario: User submits valid request
      Given User submits valid registration request
      Then User exists
      And User can log in
      And User has default Environment
      And User has Role USER

    Scenario: User submits weak password
      Given User submits weak password
        | aA1!     | short password        |
        | aaaAAA!! | no numbers            |
        | aaa111!! | no upper case letters |
        | AAA111!! | no lower case letters |
        | aaaAAA11 | no special characters |
      Then Request fails with 400
      And User doesn't exist
