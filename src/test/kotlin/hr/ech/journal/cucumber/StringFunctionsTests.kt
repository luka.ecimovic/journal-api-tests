package hr.ech.journal.cucumber

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.matches
import org.junit.Test

class StringFunctionsTests {
    @Test
    fun generatedStringHasLength10() = assertThat(randomString().length).isEqualTo(8)

    @Test
    fun generatedStringContainsLowerCaseLetters() = assertThat(randomString()).matches(Regex(".*[a-z]+.*"))

    @Test
    fun generatedStringContainsUpperCaseLetters() = assertThat(randomString()).matches(Regex(".*[A-Z]+.*"))

    @Test
    fun generatedStringContainsNumbers() = assertThat(randomString()).matches(Regex(".*\\d+.*"))
}
