@story:user-registration
Feature: User registration

    Scenario: User submits valid request
      Given User submits valid registration request
      Then User exists
      And User has default Environment
      And User has Role USER
